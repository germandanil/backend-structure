const { BaseRepository } = require('./baseRepository');

class oddsRepository extends BaseRepository {
  constructor() {
    super('odds');
  }
}
exports.oddsRepository = new oddsRepository();
