const { database } = require('../database');
const { BaseRepository } = require('./baseRepository');

class betRepository extends BaseRepository {
  constructor() {
    super('bet');
  }
  getById(eventId) {
    return database('bet')
      .where('event_id', eventId)
      .andWhere('win', null)
      .then((bets) => bets);
  }
}
exports.betRepository = new betRepository();
