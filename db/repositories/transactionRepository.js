const { BaseRepository } = require('./baseRepository');

class transactionRepository extends BaseRepository {
    constructor() {
        super('transaction');
    }
}
exports.transactionRepository = new transactionRepository();