const { database } = require('../database');

class BaseRepository {
  constructor(model) {
    this.model = model;
  }
  getAll() {
    return database(this.model)
      .select('*')
      .then((models) => models)
      .catch((error) => {
        console.log(error.detail);
        return null;
      });
  }
  getById(ID) {
    return database(this.model)
      .where('id', ID)
      .returning('*')
      .then(([model]) => model)
      .catch((error) => {
        console.log(error.detail);
        return null;
      });
  }
  update(body, id) {
    return database(this.model)
      .where('id', id)
      .update(body)
      .returning('*')
      .then(([model]) => model)
      .catch((error) => {
        console.log(error.detail);
        return null;
      });
  }
  create(body) {
    return database(this.model)
      .insert(body)
      .returning('*')
      .then(([body]) => body)
      .catch((error) => {
        console.log(error);
        return null;
      });
  }
}

exports.BaseRepository = BaseRepository;
