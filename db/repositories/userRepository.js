const ErrorCode = require('../../helper/ErrorCode');
const { database } = require('../database');
const { BaseRepository } = require('./baseRepository');

class UserRepository extends BaseRepository {
  constructor() {
    super('user');
  }
  update(body, id) {
    return database(this.model)
      .where('id', id)
      .update(body)
      .returning('*')
      .then(([model]) => model)
      .catch((err) => {
        if (err.code == '23505') {
          console.log(err.detail);
          throw new ErrorCode(err.detail, 400);
        }
        console.log(err.detail);
        throw new ErrorCode('Internal Server Error', 500);
      });
  }
  create(body) {
    body.balance = 0;
    return database(this.model)
      .insert(body)
      .returning('*')
      .then(([result]) => {
        result.createdAt = result.created_at;
        delete result.created_at;
        result.updatedAt = result.updated_at;
        delete result.updated_at;

        return { ...result };
      })
      .catch((err) => {
        if (err.code == '23505') {
          console.log(err.detail);
          throw new ErrorCode(err.detail, 400);
        }
        console.log(err.detail);
        throw new ErrorCode('Internal Server Error', 500);
      });
  }
}
exports.UserRepository = new UserRepository();
