const { BaseRepository } = require('./baseRepository');

class eventRepository extends BaseRepository {
  constructor() {
    super('event');
  }
}
exports.eventRepository = new eventRepository();
