const { eventRepository } = require("../db/repositories/eventRepository")
const { oddsRepository } = require("../db/repositories/oddsRepository")
const { betRepository } = require("../db/repositories/betRepository")
const { UserRepository } = require("../db/repositories/userRepository")
const ErrorCode = require("../helper/ErrorCode")
const {ToCamelCase,FromCamelCase} = require("../helper/properyNameChange");
const {postEvent,putEvent} = require("../db/models/events")
class eventService {
    async create(data){
          const isValidResult = postEvent.validate(data);
          if(isValidResult.error) {
            throw new ErrorCode(isValidResult.error.details[0].message,400);
          }
          try {
            data.odds=FromCamelCase(data.odds)
            let odds = await oddsRepository.create(data.odds)
            delete data.odds;
            data= FromCamelCase(data)
            let event = await eventRepository.create({
                ...data,
                odds_id: odds.id
              })
              event = ToCamelCase(event)
              odds = ToCamelCase(odds)
                return { 
                  ...event,
                  odds,
                };
          } catch (err) {
            console.log(err);
            throw new ErrorCode("Internal Server Error",500);
          }
    }
    async update(data,id){
      var isValidResult = putEvent.validate(data);
      if(isValidResult.error) {
        throw new ErrorCode(isValidResult.error.details[0].message,400);
      }
      try {
          const bets = await betRepository.getById(id)
          const [w1, w2] = data.score.split(":");
          const result = +w1 > +w2?'w1':(+w2 > +w1?'w2':'x');
          let event = await eventRepository.update({ score: data.score },id);
          bets.map(async (bet) => {
              if(bet.prediction === result) {
                await betRepository.update({win: true},bet.id)
                const user = await UserRepository.getById(bet.user_id);
                const updateData= {
                  balance: user.balance + (bet.bet_amount * bet.multiplier),
                }
                  return await UserRepository.update(updateData,bet.user_id);
              } else if(bet.prediction !== result) {
                return await betRepository.update({win: false},bet.id)
              }
            });
          event = ToCamelCase(event);
          return event;
      }
      catch (err) {
        console.log(err);
        throw new ErrorCode("Internal Server Error",500);
      }
    }
}

module.exports = new eventService()