const ErrorCode = require("../helper/ErrorCode")
const {postTransaction} = require("../db/models/transactions")



const { ToCamelCase, FromCamelCase } = require('../helper/properyNameChange');
const { UserRepository } = require("../db/repositories/userRepository");
const { transactionRepository} = require("../db/repositories/transactionRepository")
class transitionService {
    async create(data){
        console.log("))")
        const isValidResult = postTransaction.validate(data);
        if(isValidResult.error) {
          throw new ErrorCode(isValidResult.error.details[0].message,400);
        }
        const user= await UserRepository.getById(data.userId);
        if(!user) {
            throw new ErrorCode ("User does not exist",400)
        }
            data = FromCamelCase(data)
            let result = await transactionRepository.create(data)
            const currentBalance = data.amount + user.balance;
            UserRepository.update(currentBalance,data.user_id)
            result = ToCamelCase(result)
            return {...result,currentBalance};
    }

}
module.exports = new transitionService()

