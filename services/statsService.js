const { UserRepository } = require('../db/repositories/userRepository');
const { betRepository } = require('../db/repositories/betRepository');
const { eventRepository } = require('../db/repositories/eventRepository');
class UserService {
  async getStats() {
    const users = await UserRepository.getAll();
    const bets = await betRepository.getAll();
    const events = await eventRepository.getAll();
    return {
      totalUsers: users.length,
      totalBets: bets.length,
      totalEvents: events.length,
    };
  }
}

module.exports = new UserService();
