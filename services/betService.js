const ErrorCode = require("../helper/ErrorCode")
const {postBet} = require("../db/models/bets")



const { ToCamelCase, FromCamelCase } = require('../helper/properyNameChange')
const { UserRepository } = require('../db/repositories/userRepository')
const { eventRepository } = require("../db/repositories/eventRepository")
const { oddsRepository } = require("../db/repositories/oddsRepository")
const { betRepository } = require("../db/repositories/betRepository")
class betService {
    async update(data,userId){
          const isValidResult = postBet.validate(data);
          if(isValidResult.error) {
            throw new ErrorCode(isValidResult.error.details[0].message,400);
          }

          data = FromCamelCase(data);
          data.user_id = userId;
          const user = await UserRepository.getById(userId);

          if (!user) {
            throw new ErrorCode("User does not exist", 400);
          }
          if (+user.balance < +data.bet_amount) {
            throw new ErrorCode("Not enough balance", 400);
          }
          const event = await eventRepository.getById(data.event_id);
          if (!event) {
            throw new ErrorCode("Event not found", 404);
          }
          const odds = await oddsRepository.getById(event.odds_id);
          if (!odds) {
            throw new ErrorCode("Odds not found", 404);
          }
          console.log(odds);
          let multiplier;
          switch (data.prediction) {
            case "w1":
                console.log(1)
              multiplier = odds.home_win;
              break;
            case "w2":
                console.log(2)
              multiplier = odds.away_win;
              break;
            case "x":
                console.log(3)
              multiplier = odds.draw;
              break;
          }
          console.log({
            ...data,
            multiplier,
            event_id: event.id,
          })
          let bet = await betRepository.create({
            ...data,
            multiplier,
            event_id: event.id,
          });
        
          const currentBalance = user.balance - data.bet_amount;
          await UserRepository.update({ balance: currentBalance }, userId);
          bet = ToCamelCase(bet);
          return { ...bet, currentBalance: currentBalance };
          
    }
}

module.exports = new betService()