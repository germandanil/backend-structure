const { UserRepository } = require('../db/repositories/userRepository');
const ErrorCode = require('../helper/ErrorCode');
const { getUserModel, putUserModel, postUserModel } = require('../db/models/users');
class UserService {
  getById(data) {
    const isValidResult = getUserModel.validate(data);
    if (isValidResult.error) {
      throw new ErrorCode(isValidResult.error.details[0].message, 400);
    }
    return UserRepository.getById(data.id);
  }
  create(data) {
    const isValidResult = postUserModel.validate(data);
    if (isValidResult.error) {
      throw new ErrorCode(isValidResult.error.details[0].message, 400);
    }
    return UserRepository.create(data);
  }
  update(data, params, tokenPayload) {
    var isValidResult = putUserModel.validate(data);
    if (isValidResult.error) {
      throw new ErrorCode(isValidResult.error.details[0].message, 400);
    }
    if (params.id !== tokenPayload.id) {
      throw new ErrorCode('UserId mismatch', 401);
    }
    return UserRepository.update(data, params.id);
  }
}

module.exports = new UserService();
