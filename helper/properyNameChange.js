function ToCamelCase(obj) {
  let newObj = { ...obj };
  for (var key in newObj) {
    var index = key.indexOf('_');
    if (index > 0) {
      var newKey = key.replace('_', '');
      newKey = newKey.split('');
      newKey[index] = newKey[index].toUpperCase();
      newKey = newKey.join('');
      newObj[newKey] = newObj[key];
      delete newObj[key];
    }
  }
  return newObj;
}

function FromCamelCase(obj) {
  let newObj = { ...obj };
  for (var key in newObj) {
    let newKey = '';
    for (let i = 0; i < key.length; i++) {
      if (key[i].toUpperCase() === key[i]) newKey += '_' + key[i].toLowerCase();
      else newKey += key[i];
    }
    if (newKey !== key) {
      newObj[newKey] = newObj[key];
      delete newObj[key];
    }
  }
  return newObj;
}
exports.ToCamelCase = ToCamelCase;
exports.FromCamelCase = FromCamelCase;
