class ErrorCode {
  constructor(message, code) {
    this.message = message;
    this.code = code;
  }
  getError() {
    return { message: this.message, code: this.code };
  }
}
module.exports = ErrorCode;
