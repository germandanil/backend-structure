const { Router } = require('express');
const router = Router();

const jwt = require('jsonwebtoken');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { authMiddleware } = require('../middlewares/authToken.middleware');
const ErrorCode = require('../helper/ErrorCode');
const UserService = require('../services/userService');

router.get(
  '/:id',
  async (req, res, next) => {
    try {
      const result = await UserService.getById(req.params);
      if (!result) throw new ErrorCode('User not found', 404);
      res.data = result;
    } catch (err) {
      res.err = err.getError();
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.post(
  '/',
  async (req, res, next) => {
    try {
      const result = await UserService.create(req.body);
      if (!result) throw new ErrorCode('User not found', 404);

      res.data = { ...result, accessToken: jwt.sign({ id: result.id, type: result.type }, process.env.JWT_SECRET) };
    } catch (err) {
      console.log(err);
      res.err = err.getError();
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  '/:id',
  authMiddleware,
  async (req, res, next) => {
    try {
      const result = await UserService.update(req.body, req.params, res.tokenPayload);
      if (!result) throw new ErrorCode('User not found', 404);
      res.data = result;
    } catch (err) {
      console.log(err);
      if (err.getError()) res.err = err.getError();
      else res.err = { code: 404, message: `${err.message}` };
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
