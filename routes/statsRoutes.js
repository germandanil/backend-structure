const { Router } = require('express');
const router = Router();

const { responseMiddleware } = require('../middlewares/response.middleware');
const { authAdminMiddleware } = require('../middlewares/authAdminToken.middleware');
const statsService = require('../services/statsService');

router.get(
  '/',
  authAdminMiddleware,
  async (req, res, next) => {
    try {
      if (!res.err) res.data = await statsService.getStats();
    } catch (err) {
      console.log(err);
      if (err.getError()) res.err = err.getError();
      else res.err = { code: 404, message: `${err.message}` };
    } finally {
      next();
    }
  },
  responseMiddleware
);
module.exports = router;
