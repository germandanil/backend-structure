const { Router } = require('express');
const router = Router();

const { authAdminMiddleware } = require('../middlewares/authAdminToken.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const transitionService = require('../services/transactionService');

router.post(
  '/',
  authAdminMiddleware,
  async (req, res, next) => {
    try {
      if (!res.err) res.data = await transitionService.create(req.body);
    } catch (err) {
      console.log(err);
      if (err.getError()) res.err = err.getError();
      else res.err = { code: 404, message: `${err.message}` };
    } finally {
      next();
    }
  },
  responseMiddleware
);
module.exports = router;
