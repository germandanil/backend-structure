const { Router } = require('express');
const betService = require('../services/betService');
const router = Router();
const { responseMiddleware } = require('../middlewares/response.middleware');
const { authMiddleware } = require('../middlewares/authToken.middleware');

router.post(
  '/',
  authMiddleware,
  async (req, res, next) => {
    try {
      if (!res.err) {
        const id = res.tokenPayload.id;
        res.data = await betService.update(req.body, id);
      }
    } catch (err) {
      console.log(err);
      if (err.getError()) res.err = err.getError();
      else res.err = { code: 404, message: `${err.message}` };
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
