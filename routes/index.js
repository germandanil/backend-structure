const userRoutes = require('./userRoutes');
const eventRoutes = require('./eventRoutes');
const statsRouter = require('./statsRoutes');
const betsRouter = require('./betRouter');
const transactionsRouter = require('./transactionRouter');
const healthRouter = require('./healthRouter');
module.exports = (app) => {
  app.use('/users', userRoutes);
  app.use('/events', eventRoutes);
  app.use('/stats', statsRouter);
  app.use('/bets', betsRouter);
  app.use('/health', healthRouter);
  app.use('/transactions', transactionsRouter);
};
