const { Router } = require('express');
const router = Router();

const { authAdminMiddleware } = require('../middlewares/authAdminToken.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const eventService = require('../services/eventService');

router.post(
  '/',
  authAdminMiddleware,
  async (req, res, next) => {
    console.log(eventService);
    try {
      if (!res.err) {
        res.data = await eventService.create(req.body);
      }
    } catch (err) {
      console.log(err);
      res.err = err.getError();
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  '/:id',
  authAdminMiddleware,
  async (req, res, next) => {
    console.log(eventService);
    try {
      if (!res.err) {
        const eventId = req.params.id;
        res.data = await eventService.update(req.body, eventId);
      }
    } catch (err) {
      console.log(err);
      if (err.getError()) res.err = err.getError();
      else res.err = { code: 404, message: `${err.message}` };
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
