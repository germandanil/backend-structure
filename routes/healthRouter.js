const { Router } = require('express');
const router = Router();
const { postUserValidation } = require('../middlewares/user.validation');

router.get('/', postUserValidation, (req, res) => {
  res.send('Hello World!');
});

module.exports = router;
