const ErrorCode = require('../helper/ErrorCode');
const jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next) => {
  let token = req.headers['authorization'];
  if (!token) {
    const errorCode = new ErrorCode('Not Authorized', 401);
    res.err = errorCode.getError();
    next();
  } else {
    token = token.substr(7);
    try {
      res.tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
    } catch (err) {
      console.log(err);
      const errorCode = new ErrorCode('Not Authorized', 401);
      res.err = errorCode.getError();
    } finally {
      next();
    }
  }
};

exports.authMiddleware = authMiddleware;
