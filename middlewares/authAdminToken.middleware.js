const ErrorCode = require('../helper/ErrorCode');
const jwt = require('jsonwebtoken');

const authAdminMiddleware = (req, res, next) => {
  let token = req.headers['authorization'];
  if (!token) {
    const errorCode = new ErrorCode('Not Authorized', 401);
    res.err = errorCode.getError();
    next();
  } else {
    token = token.substr(7);
    try {
      const tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
      if (tokenPayload.type === 'admin') res.tokenPayload = tokenPayload;
      else throw new ErrorCode('Not admin', 403);
    } catch (err) {
      console.log(err);
      const errorCode = new ErrorCode('Not Authorized', 401);
      res.err = errorCode.getError();
    } finally {
      next();
    }
  }
};

exports.authAdminMiddleware = authAdminMiddleware;
