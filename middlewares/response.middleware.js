const responseMiddleware = (req, res, next) => {
  if (res.data) res.status(200).json(res.data);
  else if (res.err) {
    res.status(res.err.code).json({ error: res.err.message });
  } else {
    res.status(404).json('Something unpredictable has happened');
  }
  next();
};

exports.responseMiddleware = responseMiddleware;
