var express = require('express');
var knex = require('knex');

var dbConfig = require('./knexfile');
var app = express();

var port = 3000;

const { database } = require('./db/database');
app.use(express.json());
app.use((uselessRequest, uselessResponse, neededNext) => {
  database.raw('select 1+1 as result')
    .then(function () {
      neededNext();
    })
    .catch(() => {
      throw new Error('No db connection');
    });
});

const routes = require('./routes/index');
routes(app);

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});

// Do not change this line
module.exports = { app };
